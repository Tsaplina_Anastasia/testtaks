<?php
$page = file_get_contents("index.html");

if($_SERVER['REQUEST_METHOD'] == 'POST'){ 
    $dom = new domDocument;
    $dom->loadHTMLFile("index.html");
    $dom->preserveWhiteSpace = false;
    $text = $dom->getElementById('contentText')->nodeValue;

    $newText = $text;
    $searchStr = $_POST['searchStr'];
    $searchStr= trim($searchStr); 
    preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $searchStr, $matches);
    foreach ($matches as &$match) {
        foreach ($match as &$elem) {
            $elem = trim($elem,"\"");
            $newText = preg_replace('/' . $elem . '/iu', '<font color="red">\\0</font>', $newText);

        }
    }
    $page = str_replace($text, $newText, $page);
}
echo $page;
?>