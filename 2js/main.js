var formFields = ['name', 'surname', 'email'];
var userData = new Array(formFields.length);
for(var i=0; i<userData.length; i++){
    userData[i] = '';
}
var index = 0;

function isEmpty(str) {
    return (str == null) || (str.length == 0);
   }

function isEmail(str) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(str);
}

function isEmpty(str) {
    return (str == null || typeof(str) == "undefined" || str.length == 0 || str.trim() == '');
}

function previousStep() {
    if (index > 0){
        if(index == 1){
            document.getElementById("back").style.display = "none";
        }
        if(index == formFields.length-1){
            document.getElementById("complete").style.display = "none";
            document.getElementById("next").style.display = "inline-block";
        }
        index--;
        document.getElementById("input").value = userData[index];
        document.getElementById("labelForInput").innerHTML="Enter your "+formFields[index]+":";
    }
}

function nextStep() {
    if (isEmpty(document.getElementById("input").value)) {
        alert("Это поле обязательно для заполнения");
    }
    else {
        if ((formFields[index] == "email" || formFields[index] == "Email") && !isEmail(document.getElementById('input').value)) {
                alert("Некорректный email");
            }
        else {
            if (index < formFields.length){
                userData[index] = document.getElementById("input").value;
                if(index == formFields.length-1){
                    alert("Спасибо!");
                    return;
                }
                if(index == 0){
                    document.getElementById("back").style.display = "inline-block";
                }
                if(index == formFields.length-2){
                    document.getElementById("next").style.display = "none";
                    document.getElementById("complete").style.display = "inline-block";
                }
                index++;
                document.getElementById("input").value = userData[index];
                document.getElementById("labelForInput").innerHTML = "Enter your " + formFields[index] + ":";
            }
        }
    }
}
