select training.course.name, count(history_current_month.status) as quantity 
from training.course left join
(select * from training.history where training.history.status = 1 and 
date_format(FROM_UNIXTIME(training.history.end),'%Y-%m')=date_format(curdate(),'%Y-%m')) 
as history_current_month
on training.course.course_id = history_current_month.course_id
group by training.course.name;