Select training.trainee.name as trainee_name, training.trainee.email as trainee_email, 
interesting_history.course_name, interesting_history.end as history_end 
from (select history_last_month.history_id, history_last_month.trainee_id, history_last_month.course_id, 
history_last_month.end, coursePHP.name as course_name 
from (select training.history.history_id, training.history.trainee_id, training.history.course_id, 
training.history.end from training.history 
where date_format(FROM_UNIXTIME(training.history.end),'%Y-%m')=date_format(date_sub(curdate(), 
interval 1 month),'%Y-%m') and status=1) as history_last_month 
inner join (select * from training.course where training.course.name='PHP') 
as coursePHP on history_last_month.course_id=coursePHP.course_id) as interesting_history 
inner join training.trainee on 
interesting_history.trainee_id=training.trainee.trainee_id;